<!-- Titre -->

# Titre 1

## Titre 2

### Titre 3

#### Titre 4

##### Titre 5

###### Titre 6

<!-- Italique -->

_Ce texte_ est en italique

_Ce texte_ est en italique

<!-- Gras -->

**Ce texte** est en gras

**Ce texte** est en gras

<!-- Texte barré -->

~~Ce texte~~ est barré

<!-- Trait de séparation -->

---

---

<!-- Blockquote -->

> C'est un texte

<!-- Liens -->

[markdown_crash_course](https://gitlab.com/codwerk/markdown_crash_course)

[markdown_crash_course](https://gitlab.com/codwerk/markdown_crash_course "liens")

<!-- ul -->

- item 1
- item 2
- item 3
  - sous item 1
  - sous item 2

<!-- ol -->

1. item 1
2. item 2
3. item 3

<!-- Inclure du code -->

`<p>C'est un paragraphe</p>`

<!-- Images-->

![logo](https://avatars1.githubusercontent.com/u/36073831?s=460&v=4)

<!-- Blocs code -->

```bash
yarn add

yarn start
```

```javascript
const add = (num1, num2) => {
  return num1 + num2;
};
```

<!-- Tableau -->

| Name     | Email          |
| -------- | -------------- |
| John Doe | john@gmail.com |
| Jane Doe | jane@gmail.com |

<!-- Tache List -->

- [x] Task 1
- [x] Task 2
- [ ] Task 3
